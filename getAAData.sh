#!/bin/bash

# Extract data from AA
python /usr/local/ArchiveApplianceGetData/aa_scripts/getData.py -i 10.10.0.11 -f lns-vac-pv-list -z -rm -t /usr/local/ArchiveApplianceGetData/pvs/

# Delete old extracted data (older than 30 days) to prevent filling up the HDD
find /usr/local/ArchiveApplianceGetData/pvs/ -mindepth 1 -mtime +30 -delete
