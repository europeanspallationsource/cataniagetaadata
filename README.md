# Catania Vacuum System Data Backup

This is a simple collection of files to be used to obtain the backup of vacuum system data from Catania.

This relies on python scripts by Han Lee: https://github.com/jeonghanlee/epicsarchiverap-sites

1. File "getAAData.sh" is to be placed in "/usr/local/bin/" and made executable
2. Folder "aa_scripts" is to be placed in "/usr/local/ArchiveApplianceGetData/"
3. File "lns-vac-pv-list" is to be placed in "/usr/local/ArchiveApplianceGetData/aa_scripts/"
4. Cronjob is to be created ("crontab -e") with the following contents:
```
0 3 * * * export DISPLAY=:0.0 && /usr/local/bin/getAAData.sh >/dev/null 2>&1
```

As a result, the archived data will  be placed as zip-files under " /usr/local/ArchiveApplianceGetData/pvs/Archappl_10.10.0.11/", it will look something like (this is just a test, don't worry about timestamps):
```
-rw-r--r-- 1 root root 97720 Nov 24 16:41 20161124-164102.zip
-rw-r--r-- 1 root root 97890 Nov 24 16:42 20161124-164201.zip
-rw-r--r-- 1 root root 98072 Nov 24 16:43 20161124-164301.zip
-rw-r--r-- 1 root root 98284 Nov 24 16:44 20161124-164401.zip
-rw-r--r-- 1 root root 98447 Nov 24 16:45 20161124-164501.zip
-rw-r--r-- 1 root root 98711 Nov 24 16:46 20161124-164601.zip
-rw-r--r-- 1 root root 98916 Nov 24 16:47 20161124-164701.zip
```

The data can be later extracted using VPN into Catania network.

From Linux, use "scp" terminal command.

From Windows, install software called PuTTY and the following command in Windows terminal:\
```
"C:\Program Files (x86)\PuTTY\pscp.exe" -l essremotelund -pw ISrcEEEServer1 -r 172.16.30.135:/usr/local/ArchiveApplianceGetData/pvs C:\Temp\PuTTY
```
